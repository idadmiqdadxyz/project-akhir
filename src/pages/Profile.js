import React, { useState } from "react";
import { useParams } from "react-router-dom";

const Profile = () => {
  const { id } = useParams();
  const [profileData, setProfileData] = useState({
    id: "",
    name: "",
    harga: undefined,
    stock: undefined,
    image_url: "",
    is_diskon: false,
    harga_diskon: undefined,
    category: "",
    description: "",
  });

  const fetchProductById = async () => {
    try {
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/final/products/${id}`
      );
      const productData = response.data.data;
      setProductData({
        id: productData.id,
        name: productData.name,
        harga: productData.harga_display,
        stock: productData.stock,
        image_url: productData.image_url,
        is_diskon: productData.is_diskon,
        harga_diskon: productData.harga_diskon_display,
        category: productData.category,
        description: productData.description,
      });
    } catch (error) {
      console.log(error);
    }
  };
  return <div></div>;
};

export default Profile;
