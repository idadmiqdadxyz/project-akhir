import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import axios from "axios";
import { Link, useParams } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";
import { Helmet } from "react-helmet";
import { Button } from "antd";
import { CartContext } from "../context/CartContext";



const Detail = () => {
  const { id } = useParams();
  const { productData, setProductData } = useContext(ProductContext);
  const { addToCart } = useContext(CartContext);

  const fetchProductById = async () => {
    try {
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/final/products/${id}`
      );
      const productData = response.data.data;
      setProductData({
        id: productData.id,
        name: productData.name,
        harga: productData.harga,
        harga_display: productData.harga_display,
        stock: productData.stock,
        image_url: productData.image_url,
        is_diskon: productData.is_diskon,
        harga_diskon: productData.harga_diskon,
        harga_diskon_display: productData.harga_diskon_display,
        category: productData.category,
        description: productData.description,
      });
    } catch (error) {
      console.log(error);
    }
  };

  let hargaResult = 0;

  if (productData.is_diskon) {
    hargaResult = productData.harga_diskon_display;
  } else {
    hargaResult = productData.harga_display;
  }

  useEffect(() => {
    console.log(id);
    // setIdPage(id)
    fetchProductById();
  }, []);

  return (
    <div>
      <Helmet>
        <title>{productData.name}</title>
      </Helmet>
      <Navbar />
      <div className="flex justify-center">
        <div className="flex flex-col mx-24 my-12 gap-4 p-8 w-3/4 shadow-2xl rounded-2xl bg-white content-center ">
          <ul className="flex gap-2">
            <Link to={"/"}>
              <li className="text-gray-400 hover:text-blue-200">Home</li>
            </Link>
            <span>/</span>
            <Link to={"/products"}>
              <li className="text-gray-400 hover:text-blue-200">Products</li>
            </Link>
            <span>/</span>
            <li className="text-blue-300">{productData.name}</li>
          </ul>
          <div className="flex gap-8 ">
            <img
              className="w-64 h-64 flex-1 object-cover rounded-2xl"
              src={productData.image_url}
              alt="controler"
            />
            <div className="flex flex-1 gap-2 flex-col justify-between">
              <div>
                <h1 className="text-3xl font-bold leading-8">
                  {productData.name}
                </h1>
                <p className="category my-2 text-gray-400">
                  {productData.category}
                </p>
              </div>
              <div className=" my-4">
                {productData.is_diskon ? (
                  <div>
                    <p className="stroke-black">
                      <del>{productData.harga_display}</del>
                    </p>
                    <p className="text-red-500 text-2xl">
                      <strong>{productData.harga_diskon_display}</strong>
                    </p>
                  </div>
                ) : (
                  <p>
                    <strong className=" text-2xl">{productData.harga_display}</strong>
                  </p>
                )}
              </div>
              <p>{productData.description}</p>
              <div className="detail-btn flex justify-between items-center">
                <p className=" text-blue-300">Stock {productData.stock}</p>
                {localStorage.getItem("token") !== null ?
                <Button
                  onClick={() => {
                    addToCart({ ...productData, amount: 1 , harga : hargaResult});
                  }}
                  className="bg-blue-400 text-white"
                  type="primary"
                >
                  Add to cart
                </Button>
                : ""}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Detail;
