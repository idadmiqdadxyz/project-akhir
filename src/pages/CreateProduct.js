import React from 'react'
import Navbar from '../components/Navbar'
import Form from '../components/Form'
import { Helmet } from "react-helmet";

const CreateProduct = () => {
  return (
    <div>
    <Helmet>
      <title>Create Product</title>
    </Helmet>
      <Navbar />
      <Form />
    </div>
  )
}

export default CreateProduct
