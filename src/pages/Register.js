import React from "react";
import { Button, Checkbox, Form, Input } from "antd";
import { Helmet } from "react-helmet";
import Navbar from "../components/Navbar";
import axios from "axios";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Pengguna wajib diisi"),
  username: Yup.string().required("Username Pengguna wajib diisi"),
  email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
  password_confirmation: Yup.string()
    .required("Password Konfirmasi wajib diisi")
    .oneOf([Yup.ref("password"), null], "Passwords must match"),
});
const Register = () => {
  const navigate = useNavigate();

  const initialState = {
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
  };

  const onRegister = async (values) => {
    console.log(values);
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: values.name,
          username: values.username,
          email: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );
      toast.success("Register Success", {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      resetForm();
      navigate("/login");
    } catch (error) {
      toast.error(error.response.data.info, {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onRegister,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <Helmet>
        <title>Register</title>
      </Helmet>
      <Navbar />
      <div className=" flex justify-center items-center rounded-xl my-12  ">
        <div className=" bg-white flex shadow-2xl rounded-xl w-1/2 overflow-hidden justify-center">
          <div className="flex flex-col flex-end flex-1 gap-8 ">
            <h1 className="pt-8 px-8 text-4xl font-bold">Register</h1>
            <Form className=" pb-8 px-8 " name="basic">
              <label>Email</label>
              <Form.Item
                name="email"
                help={touched.email === true && errors.email}
                hasFeedback={true}
                validateStatus={errors.email === true && "errors"}
              >
                <Input
                  type="text"
                  placeholder="Masukkan email pengguna"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="email"
                  value={values.email}
                />
              </Form.Item>
              <label>Username</label>
              <Form.Item
                name="username"
                help={touched.username === true && errors.username}
                hasFeedback={true}
                validateStatus={errors.username === true && "errors"}
              >
                <Input
                  type="text"
                  placeholder="Masukkan username pengguna"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="username"
                  value={values.username}
                />
              </Form.Item>
              <label>Name</label>
              <Form.Item
                name="name"
                help={touched.name === true && errors.name}
                hasFeedback={true}
                validateStatus={errors.name === true && "errors"}
              >
                <Input
                  type="text"
                  placeholder="Masukkan nama pengguna"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="name"
                  value={values.name}
                />
              </Form.Item>
              <label>Password</label>
              <Form.Item
                name="password"
                help={touched.password === true && errors.password}
                hasFeedback={true}
                validateStatus={errors.password === true && "errors"}
              >
                <Input.Password
                  type="text"
                  placeholder="Masukkan password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="password"
                  value={values.password}
                />
              </Form.Item>
              <label>Konfirmasi Password</label>
              <Form.Item
                name="password_confirmation"
                help={
                  touched.password_confirmation === true &&
                  errors.password_confirmation
                }
                hasFeedback={true}
                validateStatus={
                  errors.password_confirmation === true && "errors"
                }
              >
                <Input.Password
                  type="text"
                  placeholder="Masukkan password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="password_confirmation"
                  value={values.password_confirmation}
                />
              </Form.Item>
              <Form.Item className="flex justify-end">
                <Button
                  className="bg-blue-400 text-white"
                  type="primary"
                  htmlType="submit"
                  onClick={handleSubmit}
                >
                  Register
                </Button>
              </Form.Item>
            </Form>
          </div>
          <div className="flex-1">
            <img
              className="h-full object-cover flex-1"
              src="https://images.unsplash.com/photo-1620987278429-ab178d6eb547?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=725&q=80"
              alt="product"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
