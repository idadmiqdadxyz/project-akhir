import React from "react";
import { Button, Checkbox, Form, Input } from "antd";
import { Helmet } from "react-helmet";
import Navbar from "../components/Navbar";
import axios from "axios";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const rulesSchema = Yup.object({
  email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
});

const Login = () => {
  const navigate = useNavigate();

  const initialState = {
    email: "",
    password: "",
  };

  const onLogin = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: values.email,
          password: values.password,
        }
      );
      console.log(response.data.data.token);
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);
      resetForm();
      toast.success("Login Sukses", {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      navigate("/table");
    } catch (error) {
      toast.error(error.response.data.info, {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onLogin,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <Navbar />
      <div className=" flex justify-center items-center rounded-xl mt-24 ">
        <div className=" bg-white flex shadow-2xl rounded-xl w-1/2 overflow-hidden justify-center">
          <div className="flex-1">
            <img
              className="h-96 w-full object-cover flex-1"
              src="https://images.unsplash.com/photo-1676030789437-e75ba37678ca?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=435&q=80"
              alt="product"
            />
          </div>
          <div className="flex flex-col flex-end flex-1 gap-8">
            <h1 className="pt-8 px-8 text-4xl font-bold">Login</h1>
            <Form className=" pb-8 px-8 " name="basic">
              <label>Email</label>
              <Form.Item
                name="email"
                help={touched.email === true && errors.email}
                hasFeedback={true}
                validateStatus={errors.email === true && "errors"}
              >
                <Input
                  type="text"
                  placeholder="Masukkan email pengguna"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="email"
                  value={values.email}
                />
              </Form.Item>
              <label>Password</label>
              <Form.Item
                name="password"
                help={touched.password === true && errors.password}
                hasFeedback={true}
                validateStatus={errors.password === true && "errors"}
              >
                <Input.Password
                  type="text"
                  placeholder="Masukkan password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="password"
                  value={values.password}
                />
              </Form.Item>
              <Form.Item className="flex justify-end">
                <Button
                  className="bg-blue-400 text-white"
                  type="primary"
                  htmlType="submit"
                  onClick={handleSubmit}
                >
                  Login
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
