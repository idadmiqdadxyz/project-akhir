import React, { useContext, useEffect, useState } from "react";
import Navbar from "../components/Navbar";
import { ProductContext } from "../context/ProductContext";
import { Link } from "react-router-dom";
import axios from "axios";
import { Helmet } from "react-helmet";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, Popconfirm, message } from "antd";

const Table = () => {
  const { fetchProduct, products } = useContext(ProductContext);
  const [filteredList, setFilteredList] = useState([...products]);
  const [isEmpty, setIsEmpty] = useState(true);
  const [first, setFirst] = useState(true);
  const [selectedCats, setSelectedCats] = useState("all");
  const [filterInput, setFilterInput] = useState("");

  useEffect(() => {
    fetchProduct();
    console.log(first);
  }, []);

  const onDelete = async (id) => {
    try {
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/final/products/${id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchProduct();
      toast.success("Delete Product Success", {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      setFirst(true);
    } catch (error) {
      toast.error("Delete Product Failed", {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const cancel = (e) => {};

  let updatedList = [...products];
  const filterBySearch = (value) => {
    if (filterInput.length !== 0 && selectedCats === "all") {
      setFirst(false);
      updatedList = updatedList.filter((item) => {
        return item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
      });
    } else if (selectedCats === "all" && filterInput === "") {
      updatedList = updatedList.filter((item) => {
        return item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
      });
    } else {
      updatedList = updatedList.filter((item) => {
        return (
          item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1 &&
          item.category === selectedCats
        );
      });
    }
    setFilteredList(updatedList);
    if (updatedList.length === 0) {
      setIsEmpty(true);
    } else {
      setIsEmpty(false);
    }
    console.log(first);
    console.log(filterInput.length);
    console.log(selectedCats);
  };

  const makeFalse = () => {
    setFirst(false);
    filterBySearch("");
  };

  useEffect(() => {
    filterBySearch(filterInput);
  }, [filterInput, selectedCats]);

  return (
    <div>
      <Helmet>
        <title>Table</title>
      </Helmet>
      <Navbar />
      <h1 className="text-2xl text-blue-400 mb-4 mt-8 text-center mx-24">
        Tabel Products
      </h1>
      <div className="relative flex flex-col items-end overflow-x-auto shadow-md sm:rounded-lg my-4 mx-24">
        <Link to={"/create"}>
          <button className="p-2 border-2 text-blue-400 border-blue-400 rounded-2xl mb-4 hover:bg-blue-400 hover:text-white">
            Create Product +
          </button>
        </Link>
        <form className="mb-8 w-full flex gap-2 p-4">
          <select
            className="border rounded-md p-1 w-32"
            name="category"
            id=""
            onClick={makeFalse}
            onChange={(e) => {
              setSelectedCats(e.target.value);
            }}
          >
            <option value="all">All</option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="hiburan">Hiburan</option>
            <option value="kendaraan">Kendaraan</option>
          </select>
          <label
            htmlFor="default-search"
            className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
          >
            Search
          </label>
          <div className="relative w-full">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                className="w-4 h-4 text-gray-500 dark:text-gray-400"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 20"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                />
              </svg>
            </div>
            <input
              type="search"
              id="default-search"
              className=" outline-none block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Cari nama product"
              onChange={(e) => {
                setFilterInput(e.target.value);
              }}
            />
          </div>
        </form>
        <table className="text-sm text-left rounded-xl overflow-hidden text-gray-500 dark:text-gray-400 ">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                Id
              </th>
              <th scope="col" className="px-6 py-3 w-12">
                Nama
              </th>
              <th scope="col" className="px-6 py-3 w-12">
                Satus Diskon
              </th>
              <th scope="col" className="px-6 py-3">
                Harga
              </th>
              <th scope="col" className="px-6 py-3 w-12">
                Harga Diskon
              </th>
              <th scope="col" className="px-6 py-3">
                Gambar
              </th>
              <th scope="col" className="px-6 py-3 w-12">
                Kategori
              </th>
              <th scope="col" className="px-6 py-3">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {first === true ? (
              products.map((product) => (
                <tr
                  key={product.id}
                  className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                >
                  <th
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    {product.id}
                  </th>
                  <td className="px-6 py-4">{product.name}</td>
                  <td className="px-6 py-4">
                    {product.is_diskon ? "Aktif" : "Tidak Aktif"}
                  </td>
                  <td className="px-6 py-4">{product.harga_display}</td>
                  <td className="px-6 py-4">{product.harga_diskon_display}</td>
                  <td className="px-6 py-4">
                    <img
                      className="w-64 h-48 object-cover flex-1 rounded-2xl"
                      src={product.image_url}
                      alt="product"
                    />
                  </td>
                  <td className="px-6 py-4">{product.category}</td>
                  <td className="px-6 py-4">
                    <Link to={`/update/${product.id}`}>
                      <button className="py-1.5 px-2 mx-1 border-none rounded-xl bg-yellow-400 text-white font-bold">
                        Update
                      </button>
                    </Link>
                    <Popconfirm
                      title="Delete Product"
                      description="Delete this product?"
                      onConfirm={() => {
                        onDelete(product.id);
                      }}
                      onCancel={cancel}
                      okText="Yes"
                      cancelText="No"
                      okType="default"
                    >
                      <Button className="text-center border-none rounded-xl bg-red-400 text-white font-bold">
                        Delete
                      </Button>
                    </Popconfirm>
                  </td>
                </tr>
              ))
            ) : isEmpty ? (
              <>
                <td></td>
                <td></td>
                <td></td>
                <h1 className="text-2xl p-24">No Item(s) Available</h1>
              </>
            ) : (
              filteredList.map((product) => (
                <tr
                  key={product.id}
                  className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                >
                  <th
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    {product.id}
                  </th>
                  <td className="px-6 py-4">{product.name}</td>
                  <td className="px-6 py-4">
                    {product.is_diskon ? "Aktif" : "Tidak Aktif"}
                  </td>
                  <td className="px-6 py-4">{product.harga_display}</td>
                  <td className="px-6 py-4">{product.harga_diskon_display}</td>
                  <td className="px-6 py-4">
                    <img
                      className="w-64 h-48 object-cover flex-1 rounded-2xl"
                      src={product.image_url}
                      alt="product"
                    />
                  </td>
                  <td className="px-6 py-4">{product.category}</td>
                  <td className="px-6 py-4">
                    <Link to={`/update/${product.id}`}>
                      <button className="py-1.5 px-2 mx-1 border-none rounded-xl bg-yellow-400 text-white font-bold">
                        Update
                      </button>
                    </Link>
                    <Popconfirm
                      title="Delete Product"
                      description="Delete this product?"
                      onConfirm={() => {
                        onDelete(product.id);
                      }}
                      onCancel={cancel}
                      okText="Yes"
                      cancelText="No"
                      okType="default"
                    >
                      <Button className="text-center border-none rounded-xl bg-red-400 text-white font-bold">
                        Delete
                      </Button>
                    </Popconfirm>
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Table;
