import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import { ProductContext } from "../context/ProductContext";
import { Link } from "react-router-dom";
import axios from "axios";
import { Helmet } from "react-helmet";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, Popconfirm, message } from "antd";
import { CartContext } from "../context/CartContext";


const Checkout = () => {
  const {
    totalBarang,
    totalHarga,
    items,
    addToCart,
    removeFromCart,
    clearCart,
  } = useContext(CartContext);

  const handleCheckout = () => {
    toast.success("Checkout Success", {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  };


  return (
    <div>
      <Helmet>
        <title>My Cart</title>
      </Helmet>
      <Navbar />
      <div className="flex justify-start items-center gap-8 ">
        <h1 className="text-2xl text-blue-400 mb-4 mt-8 text-center ml-24">
          My Cart
        </h1>
        <svg
          className=" cursor-pointer w-6 h-6 mb-4 mt-8"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          onClick={clearCart}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
          />
        </svg>
      </div>
      {items.length !== 0 ? (
      <div className="relative flex items-start justify-between gap-8 overflow-x-auto  my-4 mx-24">
        <table className="text-sm text-left text-gray-500 dark:text-gray-400 shadow-md gap-4 sm:rounded-lg">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3"></th>
              <th scope="col" className="px-6 py-3 w-24">
                Nama
              </th>
              <th scope="col" className="px-6 py-3"></th>
              <th scope="col" className="px-10 py-3">
                Harga
              </th>
              <th scope="col" className="px-10 py-3">
                Jumlah
              </th>
            </tr>
          </thead>
          <tbody>
            {items.map((product) => (
              <tr
                key={product.id}
                className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
              >
                <td className="px-10 py-4">
                  <Link Link to={`/detail/${product.id}`}>
                    <img
                      className="w-32 h-24 object-cover flex-1 rounded-2xl"
                      src={product.image_url}
                      alt="product"
                    />
                  </Link>
                </td>
                <td className="px-10 py-4 w-24">{product.name}</td>
                <td className="px-8 py-4">{product.harga}</td>
                <td className="px-8 py-4">
                  Rp.{" "}
                  {product.amount *
                    parseInt(
                      product.harga_display.replace("Rp.", "").replace(".", "")
                    )}
                </td>
                <td className="px-6 py-4">
                  <div className=" flex gap-2 justify-end">
                    <button
                      className="rounded-full bg-blue-200 text-white hover:bg-blue-300 "
                      onClick={() => {
                        removeFromCart(product);
                      }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        class="w-6 h-6"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                      </svg>
                    </button>
                    <p>{product.amount}</p>
                    <button
                      className="rounded-full bg-blue-200 text-white hover:bg-blue-300"
                      onClick={() => {
                        addToCart({ ...product, amount: 1 });
                      }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        class="w-6 h-6"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                      </svg>
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="flex jusyify-end flex-col w-96 items-end rounded-xl shadow-md p-8">
          <p className="font-bold">{totalBarang} Items</p>
          <h1 className="text-xl">
            Total Harga:{" "}
            <span className="text-2xl font-bold">
              Rp. {totalHarga.toLocaleString("en-US").replace(",", ".")}
            </span>
          </h1>
          <h1 onClick={handleCheckout} className="w-full text-center text-3xl p-4 bg-blue-300 font-bold text-white rounded-xl mt-4 cursor-pointer hover:bg-blue-400">
            Checkout
          </h1>
        </div>
      </div>
      ) : 
      (<h1 className="m-24">No item(s) on Cart</h1>)}
    </div>
  );
};

export default Checkout;
