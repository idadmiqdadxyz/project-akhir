import React from "react";
import Navbar from "../components/Navbar";
import UpdateForm from "../components/UpdateForm";
import { Helmet } from "react-helmet";

const UpdateProduct = () => {
  return (
    <div>
      <Helmet>
        <title>Update Product</title>
      </Helmet>
      <Navbar />
      <UpdateForm />
    </div>
  );
};

export default UpdateProduct;
