import { createContext, useReducer, useState } from "react";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const CartContext = createContext();

const initialState = {
  items: [],
  totalBarang: 0,
  totalHarga: 0,
};

const cartReducer = (state, action) => {
  if (action.type === "ADD_ITEM") {
    const updateTotalHarga =
      state.totalHarga +
      parseInt(action.payload.harga.replace("Rp.", "").replace(".", ""));
    console.log(updateTotalHarga);
    const existCartItemIdx = state.items.findIndex(
      (item) => item.id === action.payload.id
    );
    const existingItem = state.items[existCartItemIdx];
    let updatedItems;

    if (existingItem) {
      const updatedItem = {
        ...existingItem,
        amount: existingItem.amount + action.payload.amount,
      };
      updatedItems = [...state.items];
      updatedItems[existCartItemIdx] = updatedItem;
      console.log(updatedItem.amount);
    } else {
      updatedItems = state.items.concat(action.payload);
    }

    return {
      items: updatedItems,
      totalHarga: updateTotalHarga,
      totalBarang: state.totalBarang + 1,
    };
  }
  if (action.type === "REMOVE_ITEM") {
    const updateTotalHarga =
      state.totalHarga -
      parseInt(action.payload.harga.replace("Rp.", "").replace(".", ""));

    const existCartItemIdx = state.items.findIndex(
      (item) => item.id === action.payload.id
    );
    const existingItem = state.items[existCartItemIdx];
    let updatedItems;

    if (existingItem.amount === 1) {
      updatedItems = state.items.filter(
        (item) => item.id !== action.payload.id
      );
    } else if (existingItem) {
      const updatedItem = {
        ...existingItem,
        amount: existingItem.amount - 1,
      };
      updatedItems = [...state.items];
      updatedItems[existCartItemIdx] = updatedItem;
      console.log(updatedItem.amount);
    } else {
      updatedItems = state.items.concat(action.payload);
    }

    return {
      items: updatedItems,
      totalHarga: updateTotalHarga,
      totalBarang: state.totalBarang - 1,
    };
  }
  if (action.type === "CLEAR_ITEM") {
    return {
      items: [],
      totalBarang: 0,
      totalHarga: 0,
    };
  }
  return initialState;
};

export function CartProvider({ children }) {
  const [cartItem, cartDispatch] = useReducer(cartReducer, initialState);

  const addToCart = (item) => {
    console.log(item);
    toast.success("Item Added", {
      position: "top-center",
      autoClose: 300,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    cartDispatch({ type: "ADD_ITEM", payload: item });
  };

  const removeFromCart = (item) => {
    console.log(item);
    toast.info("Item Removed", {
      position: "top-center",
      autoClose: 300,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    cartDispatch({ type: "REMOVE_ITEM", payload: item });
  };

  const clearCart = (item) => {
    console.log(item);
    toast.info("Cart Clear", {
      position: "top-center",
      autoClose: 300,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    cartDispatch({ type: "CLEAR_ITEM" });
  };

  return (
    <CartContext.Provider
      value={{
        items: cartItem.items,
        totalBarang: cartItem.totalBarang,
        totalHarga: cartItem.totalHarga,
        addToCart,
        clearCart,
        removeFromCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
}
