import axios from "axios";
import { createContext, useState } from "react";

export const ProductContext = createContext();

export function ContextProvider({ children }) {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [productData, setProductData] = useState({
    id: "",
    name: "",
    harga: undefined,
    harga_display: undefined,
    stock: undefined,
    image_url: "",
    is_diskon: false,
    harga_diskon: undefined,
    harga_diskon_display: undefined,
    category: "",
    description: "",
  });

  const fetchProduct = async () => {
    setIsLoading(true);
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setIsLoading(false);
      setProducts(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ProductContext.Provider
      value={{
        products,
        setProducts,
        fetchProduct,
        productData,
        setProductData,
        isLoading,
        setIsLoading,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
}
