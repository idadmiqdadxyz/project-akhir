import { Button } from "antd";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { CartContext } from "../context/CartContext";

const Product = ({ product }) => {
  let hargaResult = 0;

  if (product.is_diskon) {
    hargaResult = product.harga_diskon_display;
  } else {
    hargaResult = product.harga_display;
  }
  const { addToCart } = useContext(CartContext);
  return (
    <div className=" h-96 w-64 rounded-2xl overflow-hidden shadow-lg transition duration-300 ease-in-out hover:scale-105">
      <div className="relative">
        <Link to={`/detail/${product.id}`}>
          <img
            className="w-64 h-48 object-cover flex-1 cursor-pointer "
            src={product.image_url}
            alt="product"
          />
          <span className="py-1 px-2 font-bold rounded-xl text-sm bg-white absolute bottom-2 left-2">
            {product.category}
          </span>
        </Link>
      </div>
      <div className="p-4 flex h-48 justify-between flex-1 flex-col gap-4">
        <h3 className="text-1xl font-bold leading-4">{product.name.slice(0, 60)}</h3>

        {product.is_diskon ? (
          <div>
            <p className="stroke-black">
              <del>{product.harga_display}</del>
            </p>
            <p className="text-red-500">
              <strong>{product.harga_diskon_display}</strong>
            </p>
          </div>
        ) : (
          <p>
            <strong>{product.harga_display}</strong>
          </p>
        )}
        <div className="flex justify-between items-center ">
          <p className="text-sm text-blue-400">Stock {product.stock}</p>
          {localStorage.getItem("token") !== null ? (
            <Button
              onClick={() => {
                addToCart({ ...product, amount: 1, harga: hargaResult });
              }}
              className="bg-blue-400 text-white"
              type="primary"
            >
              Add to cart
            </Button>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};

export default Product;
