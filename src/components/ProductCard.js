import React from "react";
import { Link } from "react-router-dom";

const ProductCard = ({ product }) => {
  return (
    <Link to={`/detail/${product.id}`}>
      <div className=" h-64 w-64 rounded-2xl overflow-hidden shadow-lg transition duration-300 ease-in-out hover:scale-105 cursor-pointer">
        <div className="relative">
          <img
            className="w-64 h-64 object-cover  "
            src={product.image_url}
            alt="product"
          />
          <span className="py-1 px-2 font-bold rounded-xl text-sm bg-white absolute top-3 left-3">
            {product.category}
          </span>
          <div className="p-4 flex items-center w-full bg-white/60 rounded-xl absolute bottom-0 h-24 justify-start flex-col">
            <h3 className="text-1xl font-bold leading-4">{product.name.slice(0,20)}</h3>

            {product.is_diskon ? (
              <div className=" text-center justify-center gap-0 items-center">
                <p className="stroke-black mb-0">
                  <del className="text-xs">{product.harga_display}</del>
                </p>
                <p className="text-red-500 text-md mt-0">
                  <strong>{product.harga_diskon_display}</strong>
                </p>
              </div>
            ) : (
                <strong className="p-2">{product.harga_display}</strong>
            )}
            <div className=" absolute bottom-2 right-3 justify-between items-center ">
              <p className="text-xs text-blue-400">Stock {product.stock}</p>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default ProductCard;
