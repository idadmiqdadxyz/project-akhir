import React, { useContext, useState } from "react";
import { CartContext } from "../context/CartContext";
import { Button, Modal } from "antd";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Navigate, useNavigate } from "react-router-dom";

const Cart = () => {
  const navigate = useNavigate();
  const {
    totalBarang,
    totalHarga,
    items,
    addToCart,
    removeFromCart,
    clearCart,
  } = useContext(CartContext);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    navigate("/checkout");
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className="relative">
      <button onClick={showModal}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="w-8 h-8"
          onClick={showModal}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
          />
        </svg>
      </button>
      <Modal
        title="Your Cart"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancle
          </Button>,
          <Button
            className=" bg-blue-300"
            key="link"
            type="primary"
            onClick={handleOk}
          >
            Checkout
          </Button>,
        ]}
      >
        <div className="flex flex-col">
          {items.map((item) => (
            <div className="grid grid-cols-3 mb-1" key={item.id}>
              <p>{item.name.slice(0, 20)}</p>
              <span className=" mx-8">
                Rp.{" "}
                {item.amount *
                  parseInt(item.harga.replace("Rp.", "").replace(".", ""))}
              </span>
              <div className=" flex gap-2 justify-end">
                <button
                  className="rounded-full bg-blue-200 text-white hover:bg-blue-300 "
                  onClick={() => {
                    removeFromCart(item);
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                </button>
                <p>{item.amount}</p>
                <button
                  className="rounded-full bg-blue-200 text-white hover:bg-blue-300"
                  onClick={() => {
                    addToCart({ ...item, amount: 1 });
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          ))}
          <div className="flex justify-between items-center">
            <svg
              className=" cursor-pointer w-6 h-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              onClick={clearCart}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
              />
            </svg>

            <h2 className="flex text-xl justify-end">
              Total : Rp. {totalHarga.toLocaleString("en-US").replace(",", ".")}
            </h2>
          </div>
        </div>
      </Modal>

      <span className="bg-blue-400 rounded-full px-2 text-sm absolute -top-1 -right-3">
        {totalBarang}
      </span>
    </div>
  );
};

export default Cart;
