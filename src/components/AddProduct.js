import axios from "axios";
import React, { useState } from "react";

const AddProduct = ({ fetchProduct }) => {
  const [productData, setProductData] = useState({
    name: "",
    harga: undefined,
    stock: undefined,
    image_url: "",
    is_diskon: false,
    harga_diskon: undefined,
    category: "",
    description: "",
  });

  const changeHandler = (e) => {
    setProductData((prevInput) => ({
      ...prevInput,
      [e.target.name]: e.target.value,
    }));

    console.log(productData);
  };

  const toggleHandler = (e) => {
    setProductData((prevInput) => ({
      ...prevInput,
      is_diskon: e.target.checked,
    }));
    console.log(productData);
  };

  const cancleHnadler = () => {
    setProductData({
      name: "",
      harga: undefined,
      stock: undefined,
      image_url: "",
      is_diskon: false,
      harga_diskon: undefined,
      category: "",
      description: "",
    });
  };

  const submitHandler = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/products",
        {
          name: productData.name,
          harga: productData.harga,
          stock: productData.stock,
          image_url: productData.image_url,
          is_diskon: productData.is_diskon,
          harga_diskon: productData.harga_diskon,
          category: productData.category,
          description: productData.description,
        }
      );
      fetchProduct();
      setProductData({
        name: "",
        harga: undefined,
        stock: undefined,
        image_url: "",
        is_diskon: false,
        harga_diskon: undefined,
        category: "",
        description: "",
      });
      alert("Berhasil Menambah Produuct");
    } catch (error) {
      alert(error.response.data.info);
    }
  };

  return (
    <div className="bg-white shadow-xl rounded-2xl p-6 my-8 mx-24">
      <section>
        <h2 className="text-2xl text-blue-400 my-4">Create Product</h2>
      </section>
      <form className="grid grid-cols-5 gap-3">
        <div className="flex flex-col col-span-3 gap-1">
          <label>Nama Barang</label>
          <input
            name="name"
            className="border rounded-md p-1"
            type="text"
            placeholder="Masukan Nama Barang"
            value={productData.name}
            onChange={changeHandler}
          />
        </div>
        <div className="flex flex-col col-span-2 gap-1">
          <label>Stock Barang</label>
          <input
            name="stock"
            className="border rounded-md p-1"
            type="number"
            placeholder="Masukan Jumlah Barang"
            value={productData.stock === undefined ? "" : productData.stock}
            onChange={changeHandler}
          />
        </div>
        <div className="flex flex-col col-span-2 gap-1">
          <label>Harga Barang</label>
          <input
            name="harga"
            className="border rounded-md p-1"
            type="number"
            placeholder="Masukan Harga Barang"
            value={productData.harga === undefined ? "" : productData.harga}
            onChange={changeHandler}
          />
        </div>
        <div className="flex justify-center items-center gap-2 pt-6">
          <input
            type="checkbox"
            id="discount"
            name="is_diskon"
            checked={productData.is_diskon}
            onChange={toggleHandler}
          />
          <label for="discount"> Status Dicount</label>
        </div>
        {productData.is_diskon ? (
          <div className="flex flex-col col-span-2 gap-1">
            <label>Harga Diskon</label>
            <input
              name="harga_diskon"
              className="border rounded-md p-1"
              type="number"
              placeholder="Masukan Harga Diskon"
              value={
                productData.harga_diskon === undefined
                  ? ""
                  : productData.harga_diskon
              }
              onChange={changeHandler}
            />
          </div>
        ) : (
          <div className="col-span-2"></div>
        )}

        <div className="flex flex-col col-span-2 gap-1">
          <label>Kategori</label>
          <select
            className="border rounded-md p-1"
            name="category"
            id=""
            value={productData.category}
            onChange={changeHandler}
          >
            <option value="" disabled>
              Pilih Kategori
            </option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="hiburan">Hiburan</option>
            <option value="kendaraan">Kendaraan</option>
          </select>
        </div>
        <div className="flex flex-col col-span-3 gap-1">
          <label>Gambar Barang</label>
          <input
            name="image_url"
            className="border rounded-md p-1"
            type="text"
            placeholder="Masukan URL Gambar"
            value={productData.image_url}
            onChange={changeHandler}
          />
        </div>
        <div className="flex flex-col col-span-5 gap-1">
          <label>Deskripsi</label>
          <textarea
            name="description"
            className="border rounded-md p-1"
            placeholder="masukan deskripsi"
            cols="50"
            rows="4"
            value={productData.description}
            onChange={changeHandler}
          ></textarea>
        </div>
        <div className="flex gap-2 col-start-5 justify-end">
          <button
            onClick={cancleHnadler}
            className="py-2 px-4 rounded-xl border-2 border-blue-400 text-blue-400"
            type="button"
          >
            Cancle
          </button>
          <button
            onClick={submitHandler}
            className="py-2 px-4 rounded-xl bg-blue-400 text-white"
            type="button"
          >
            Create
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddProduct;
