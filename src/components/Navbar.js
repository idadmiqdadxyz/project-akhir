import React, { useContext, useState } from "react";
import images from "../assets/logoipsum-260.svg";
import { Link, NavLink, useNavigate } from "react-router-dom";
import Cart from "./Cart";
import { DownOutlined, SmileOutlined } from "@ant-design/icons";
import { Dropdown, Space, Button } from "antd";
import axios from "axios";
import { CartContext } from "../context/CartContext";

const Navbar = () => {
  const navigate = useNavigate();
  const { clearCart } = useContext(CartContext);

  const onLogout = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    } finally {
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      clearCart();
      navigate("/login");
    }
  };

  const items = [
    {
      key: "1",
      label: (
        
        <Link
          className="flex gap-2 px-3"
          to={"/checkout"}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-6 h-6"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"
            />
          </svg>
          My Cart
        </Link>
      ),
    },
    {
      key: "2",
      label: (
        <Button
          className="flex gap-2"
          onClick={onLogout}
          type="primary"
          danger
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-6 h-6"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
            />
          </svg>
          Logout
        </Button>
      ),
    },
  ];

  return (
    <div className="grid grid-cols-3 px-24 py-4 shadow-lg sticky top-0 bg-white z-10">
      <div className="">
        <img className="h-12" src={images} alt="logo" />
      </div>
      <ul className="flex gap-4 justify-center items-center">
        <li className="text-xl text-gray-400 hover:text-blue-200">
          <NavLink
            to="/"
            className={({ isActive }) => (isActive ? "text-blue-400" : "")}
          >
            Home
          </NavLink>
        </li>
        <li className="text-xl text-gray-400 hover:text-blue-200">
          <NavLink
            to="/products"
            className={({ isActive }) => (isActive ? "text-blue-400" : "")}
          >
            Products
          </NavLink>
        </li>
        <li className="text-xl text-gray-400 hover:text-blue-200">
          <NavLink
            to="/table"
            className={({ isActive }) => (isActive ? "text-blue-400" : "")}
          >
            Table
          </NavLink>
        </li>
      </ul>
      <div className="flex gap-4 justify-end items-center">
        {localStorage.getItem("token") !== null ? (
          <>
            <Cart />
            <Dropdown
              className=" cursor-pointer "
              menu={{
                items,
              }}
            >
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  <h1 className="text-xl">
                    Hello! {localStorage.getItem("username")}
                  </h1>
                  <DownOutlined />
                </Space>
              </a>
            </Dropdown>
          </>
        ) : (
          <>
            <NavLink
              to="/login"
              className={({ isActive }) =>
                isActive ? "text-blue-400" : "hover:text-blue-200"
              }
            >
              <button>Login</button>
            </NavLink>
            <NavLink
              to="/register"
              className={({ isActive }) =>
                isActive ? "text-blue-400" : "hover:text-blue-200"
              }
            >
              <button>Register</button>
            </NavLink>
          </>
        )}
      </div>
    </div>
  );
};

export default Navbar;
