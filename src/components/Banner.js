import React, { useContext } from "react";
import { Carousel } from "antd";
import { ProductContext } from "../context/ProductContext";
import { Link } from "react-router-dom";
import Achievements from "./Achievements";
import image2 from "../assets/emy-XoByiBymX20-unsplash.jpg";
import image3 from "../assets/rajasekhar-r-O-yvgclSKM8-unsplash.jpg";
import image4 from "../assets/alex-litvin-MAYsdoYpGuk-unsplash.jpg";
import image5 from "../assets/campbell-3ZUsNJhi_Ik-unsplash.jpg";

const Banner = () => {
  const { products } = useContext(ProductContext);

  const produkTech = products
    .filter((product) => product.category === "teknologi")
    .map((filteredProduct) => {
      return filteredProduct;
    });

  const produkFood = products
    .filter((product) => product.category === "makanan")
    .map((filteredProduct) => {
      return filteredProduct;
    });

  const produkDrinks = products
    .filter((product) => product.category === "minuman")
    .map((filteredProduct) => {
      return filteredProduct;
    });

  const produkEntertain = products
    .filter((product) => product.category === "hiburan")
    .map((filteredProduct) => {
      return filteredProduct;
    });

  const produkVehic = products
    .filter((product) => product.category === "kendaraan")
    .map((filteredProduct) => {
      return filteredProduct;
    });

  const bannerItems = [
    {
      name: "Teknologi",
      desc: "Penuhi kebutuhan akan teknologi yang terus berkembang",
      image:
        "https://images.unsplash.com/photo-1556573559-f1c05d66f273?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
      filter: produkTech,
    },
    {
      name: "Makanan",
      desc: "Nikmati berbagai makanan yang dapat memuaskan rasa lapar",
      image: image2,
      filter: produkFood,
    },
    {
      name: "Minuman",
      desc: "Puaskan dahaga anda dengan kesegaran yang menyejukan",
      image: image3,
      filter: produkDrinks,
    },
    {
      name: "Hiburan",
      desc: "Hiburlah diri anda dari kepenatan selama ini",
      image: image4,
      filter: produkEntertain,
    },
    {
      name: "Kendaraan",
      desc: "Yes, brummm brumm",
      image: image5,
      filter: produkVehic,
    },
  ];
  return (
    <div className="relative flex flex-col mb-48">
      <Carousel autoplay className="h-96">
        {bannerItems.map((item) => (
          <div key={item.name} className="relative">
            <img
              className="w-full h-banner object-cover flex-1"
              src={item.image}
              alt="product"
            />
            <div className="absolute flex flex-col bottom-32 mx-24 gap-4">
              <h1 className="text-white text-4xl font-bold">{item.name}</h1>
              <h2 className="text-white text-2xl w-96">{item.desc}</h2>
              <div className="flex gap-4">
                {item.filter.slice(0, 2).map((product) => (
                  <Link key={product.id} to={`/detail/${product.id}`}>
                    <div className=" cursor-pointer rounded-xl overflow-hidden relative bg-white transition duration-300 ease-in-out hover:scale-110">
                      <img
                        className="h-24 w-48 object-cover flex-1"
                        src={product.image_url}
                        alt="product"
                      />
                      <div className=" bg-gradient-to-t flex flex-col justify-end w-full h-full absolute p-2 bottom-0 from-white from-10% via-transparent">
                        <h4 className="text-md w-36 p-1 font-bold leading-4 overflow-hidden">
                          {product.name.slice(0, 20)}
                        </h4>
                      </div>
                    </div>
                  </Link>
                ))}
              </div>
            </div>
          </div>
        ))}
      </Carousel>
      <Achievements />
    </div>
  );
};

export default Banner;
