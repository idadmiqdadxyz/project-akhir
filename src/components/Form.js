import React, { useContext, useState } from "react";
import { ProductContext } from "../context/ProductContext";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const validationSchema = Yup.object({
  name: Yup.string().required("Nama wajib diisi"),
  harga: Yup.number().required("Harga wajib diisi"),
  stock: Yup.number().required("Stock wajib diisi"),
  image_url: Yup.string().required("Gambar wajib diisi").url("URL tidak valid"),
  is_diskon: Yup.boolean(),
  harga_diskon: Yup.number().when("is_diskon", {
    is: true,
    then: () =>
      Yup.number().required("Harga Diskon wajib diisi ketika diskon menyala"),
  }),
  category: Yup.string().required("Kategori wajib diisi"),
});

const Form = () => {
  const navigate = useNavigate();
  const { fetchProduct } = useContext(ProductContext);
  const initialState = {
    name: "",
    harga: undefined,
    stock: undefined,
    image_url: "",
    is_diskon: false,
    harga_diskon: undefined,
    category: "",
    description: "",
  };

  const onSubmit = async (values) => {
    console.log(values);
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          name: values.name,
          harga: values.harga,
          stock: values.stock,
          image_url: values.image_url,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          description: values.description,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchProduct();
      toast.success("Produk Sukses Dibuat", {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      navigate("/table");
    } catch (error) {
      toast.error(error.response.data.info, {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const {
    handleChange,
    values,
    handleSubmit,
    errors,
    handleReset,
    handleBlur,
    touched,
  } = useFormik({
    initialValues: initialState,
    validationSchema: validationSchema,
    onSubmit: onSubmit,
  });

  return (
    <div className="bg-white shadow-xl rounded-2xl p-6 my-8 mx-24">
      <section>
        <h2 className="text-2xl text-blue-400 my-4">Create Product</h2>
      </section>
      <form className="grid grid-cols-5 gap-3">
        <div className="flex flex-col col-span-3 gap-1">
          <label>Nama Barang</label>
          <input
            name="name"
            className="border rounded-md p-1"
            type="text"
            placeholder="Masukan Nama Barang"
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <p className="text-red-600">{touched.name === true && errors.name}</p>
        </div>
        <div className="flex flex-col col-span-2 gap-1">
          <label>Stock Barang</label>
          <input
            name="stock"
            className="border rounded-md p-1"
            type="number"
            placeholder="Masukan Jumlah Barang"
            value={values.stock === undefined ? "" : values.stock}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <p className="text-red-600">
            {touched.stock === true && errors.stock}
          </p>
        </div>
        <div className="flex flex-col col-span-2 gap-1">
          <label>Harga Barang</label>
          <input
            name="harga"
            className="border rounded-md p-1"
            type="number"
            placeholder="Masukan Harga Barang"
            value={values.harga === undefined ? "" : values.harga}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <p className="text-red-600">
            {touched.harga === true && errors.harga}
          </p>
        </div>
        <div className="flex justify-center items-center gap-2 pt-6">
          <input
            type="checkbox"
            id="discount"
            name="is_diskon"
            checked={values.is_diskon}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <label htmlFor="discount"> Status Dicount</label>
        </div>
        {values.is_diskon ? (
          <div className="flex flex-col col-span-2 gap-1">
            <label>Harga Diskon</label>
            <input
              name="harga_diskon"
              className="border rounded-md p-1"
              type="number"
              placeholder="Masukan Harga Diskon"
              value={
                values.harga_diskon === undefined ? "" : values.harga_diskon
              }
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <p className="text-red-600">
              {touched.harga_diskon === true && errors.harga_diskon}
            </p>
          </div>
        ) : (
          <div className="col-span-2"></div>
        )}

        <div className="flex flex-col col-span-2 gap-1">
          <label>Kategori</label>
          <select
            className="border rounded-md p-1"
            name="category"
            id=""
            value={values.category}
            onChange={handleChange}
            onBlur={handleBlur}
          >
            <option value="" disabled>
              Pilih Kategori
            </option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="hiburan">Hiburan</option>
            <option value="kendaraan">Kendaraan</option>
          </select>
          <p className="text-red-600">
            {touched.category === true && errors.category}
          </p>
        </div>
        <div className="flex flex-col col-span-3 gap-1">
          <label>Gambar Barang</label>
          <input
            name="image_url"
            className="border rounded-md p-1"
            type="text"
            placeholder="Masukan URL Gambar"
            value={values.image_url}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <p className="text-red-600">
            {touched.image_url === true && errors.image_url}
          </p>
        </div>
        <div className="flex flex-col col-span-5 gap-1">
          <label>Deskripsi</label>
          <textarea
            name="description"
            className="border rounded-md p-1"
            placeholder="masukan deskripsi"
            cols="50"
            rows="4"
            value={values.description}
            onChange={handleChange}
            onBlur={handleBlur}
          ></textarea>
        </div>
        <div className="flex gap-2 col-start-5 justify-end">
          <button
            onClick={handleReset}
            className="py-2 px-4 rounded-xl border-2 border-blue-400 text-blue-400"
            type="button"
          >
            Cancel
          </button>
          <button
            onClick={handleSubmit}
            className="py-2 px-4 rounded-xl bg-blue-400 text-white"
            type="button"
          >
            Create
          </button>
        </div>
      </form>
    </div>
  );
};

export default Form;
