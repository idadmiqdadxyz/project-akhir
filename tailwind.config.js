/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    extend: {
      height: {
        'banner': '30rem',
      },
      colors: {
        'custom-dark-blue':'#001C30',
      }
    },
  },
  plugins: [],
}

